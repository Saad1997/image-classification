
import numpy.matlib as npm
import numpy as np
import math
from PIL import Image, ImageDraw
from math import degrees, radians, sin, cos, atan, sqrt
import cv2
import os
class DataProcessing:
    def __init__(self):
        pass

    def resize_grasps( r_grasps,xc, yc, w, h, angle, resized_grasps=[]):
        Rx = 640/1536
        Ry = 480/1536
        xc_ = xc*Rx
        yc_ = yc*Ry
        #w_ = w*Rx
        w_=15
        height=[]
        grasps=[]
        h_ = float(h*Ry)
        angle_ = float(angle)
        print({"height":h_})
        
        if h_ > 12:
    #        h_=20#lets make h a fix value so that the centre of rectangles obtained by spliting big rectangle remia same to centre of big rectangle  
            
            h1 = float((h_) / 2)
            h2 = float(h_ - h1)
            height = [h1, h2]
                
                
            bbox = npm.repmat([[xc_], [yc_]], 1, 5) + \
                    np.matmul([[math.cos(angle_), -math.sin(angle_)],
                                [math.sin(angle_), math.cos(angle_)]],
                                [[-w_ / 2, w_ / 2, w_ / 2, -w_ / 2, w_ / 2 +8],
                                [-h_ / 2, -h_ / 2, h_ / 2, h_ / 2, 0]])
            # add first point

            x1, y1 = bbox[0][0], bbox[1][0]
                    # add second point
            x2, y2 = bbox[0][1], bbox[1][1]
                    # add forth point
            x3, y3 = bbox[0][2], bbox[1][2]
                    # add fifth point
            x4, y4 = bbox[0][3], bbox[1][3]
            resized_grasps=[x1, y1, x2, y2, x3, y3, x4, y4]
            print(resized_grasps)
            grasps.append(resized_grasps)
            grasp = str(x1) + " " + str(y1) + "\n" + str(x2) + " " + str(y2) + "\n" + str(x3) + " " + str(y3) + "\n" + str(x4) + " " + str(y4) + "\n"
            r_grasps.write(grasp)        
            
            for i in range(0, len(height)):

                print("in_if")
                bbox = npm.repmat([[xc_], [yc_]], 1, 5) + \
                    np.matmul([[math.cos(angle_), -math.sin(angle_)],
                                [math.sin(angle_), math.cos(angle_)]],
                                [[-w_ / 2, w_ / 2, w_ / 2, -w_ / 2, w_ / 2 + 8],
                                [-h_ / 2, -h_ / 2, h_ / 2, h_ / 2, 0]])
                print({"bbox": bbox})
                if (i == 1):

                    # add second point
                    x1 = xc_ - ((w_ / 2) * math.cos(angle))
                    y1 = yc_ - ((w_ / 2) * math.sin(angle))

                    x2 = xc_ + ((w_ / 2) * math.cos(angle))
                    y2 = yc_ + ((w_ / 2) * math.sin(angle))

                    x3, y3 = bbox[0][2], (bbox[1][2])
                    x4, y4 = bbox[0][3], (bbox[1][3])


                else:
                    # add first point
                    x1, y1 = bbox[0][0], bbox[1][0]

                    x2, y2 = bbox[0][1], bbox[1][1]

                    x3 = xc_ + ((w_ / 2) * math.cos(angle))
                    y3 = yc_ + ((w_ / 2) * math.sin(angle))

                    x4 = xc_ - ((w_ / 2) * math.cos(angle))
                    y4 = yc_ - ((w_ / 2) * math.sin(angle))

                resized_grasps = [x1, y1, x2, y2, x3, y3, x4, y4]

                print(resized_grasps)
                grasps.append(resized_grasps)
                grasp = str(x1) + " " + str(y1) + "\n" + str(x2) + " " + str(y2) + "\n" + str(x3) + " " + str(y3) + "\n" + str(x4) + " " + str(y4) + "\n"
                r_grasps.write(grasp)

        else:
            print("in_else")
            bbox = npm.repmat([[xc_], [yc_]], 1, 5) + \
                np.matmul([[math.cos(angle_), -math.sin(angle_)],
                            [math.sin(angle_), math.cos(angle_)]],
                            [[-w_ / 2, w_ / 2, w_ / 2, -w_ / 2, w_ / 2 +8],
                            [-h_ / 2, -h_ / 2, h_ / 2, h_ / 2, 0]])
            # add first point

            x1, y1 = bbox[0][0], bbox[1][0]
                    # add second point
            x2, y2 = bbox[0][1], bbox[1][1]
                    # add forth point
            x3, y3 = bbox[0][2], bbox[1][2]
                    # add fifth point
            x4, y4 = bbox[0][3], bbox[1][3]

            resized_grasps=[x1, y1, x2, y2, x3, y3, x4, y4]
            print(resized_grasps)
            grasps.append(resized_grasps)
            grasp = str(x1) + " " + str(y1) + "\n" + str(x2) + " " + str(y2) + "\n" + str(x3) + " " + str(y3) + "\n" + str(x4) + " " + str(y4) + "\n"
            r_grasps.write(grasp)
        return grasps


    def resize_img(rgb_save_path,img):
        img_path=os.path.join(rgb_save_path,img)
        print({"image_path":img_path})
        img_array = cv2.imread(img_path)
        resized_img = cv2.resize(img_array, (640, 480), interpolation=cv2.INTER_CUBIC)
        path__ = os.path.split(img_path)
        cv2.imwrite(os.path.join(rgb_save_path,img),resized_img)
        return resized_img


    def rename_img(grasp_file_name,  save_path, k):

        image_file_name=grasp_file_name.replace(".txt",".png")
        print(image_file_name)
        img_= cv2.imread(image_file_name)
        img=cv2.imwrite((save_path+os.path.split(image_file_name)[1]),img_)
        image_name=os.path.split(image_file_name)[1]
        print(image_name)
        if 0<=k<=9:
            os.rename(save_path+image_name,save_path+"/"+"pcd"+"000"+str(k)+"r.png")
            image = "pcd" + "000" + str(k) + "r.png"
            k+=1
        elif 10<=k<=99:
            os.rename(save_path+image_name,save_path+"/"+"pcd" + "00" + str(k) + "r.png")
            image = "pcd" + "00" + str(k) + "r.png"
            k+=1
        else:
            os.rename(save_path+image_name,save_path+"/"+"pcd" + "0" + str(k) + "r.png")
            image = "pcd" + "0" + str(k) + "r.png"
            k += 1

        return image

    def draw_grasps(grasps, image):
        img__=np.empty([480, 640])
        img = cv2.imread(image)
        #print({"graspsss are":grasps})
        for grasp in grasps:
            for i in (0, (len(grasp)-1)):
                p5 = (int(grasp[i][0]), int(grasp[i][1]))
                p6 = (int(grasp[i][2]), int(grasp[i][3]))
                p7 = (int(grasp[i][4]), int(grasp[i][5]))
                p8 = (int(grasp[i][6]), int(grasp[i][7]))

                img__ = cv2.line(img, p5, p6, (10, 200, 200), 2)
                img__ = cv2.line(img, p8, p7, (10, 200, 200), 2)
                img__ = cv2.line(img, p5, p8, (10, 200, 200), 2)
                img__ = cv2.line(img, p6, p7, (10, 200, 200), 2)
        return img__































