import warnings
from datetime import datetime
from utils.dataset_processing import evaluation, grasp
import matplotlib.pyplot as plt
import numpy as np 
from cv2 import cv2
from utils.dataset_processing.grasp import detect_grasps

warnings.filterwarnings("ignore")


def plot_results(
        fig,
        rgb_img,
        grasp_q_img,
        grasp_angle_img,
        depth_img=None,
        no_grasps=1,
        grasp_width_img=None
):
    """
    Plot the output of a network
    :param fig: Figure to plot the output
    :param rgb_img: RGB Image
    :param depth_img: Depth Image
    :param grasp_q_img: Q output of network
    :param grasp_angle_img: Angle output of network
    :param no_grasps: Maximum number of grasps to plot
    :param grasp_width_img: (optional) Width output of network
    :return:
    """
    gs = detect_grasps(grasp_q_img, grasp_angle_img, width_img=grasp_width_img, no_grasps=no_grasps)

    plt.ion()
    plt.clf()
    ax = fig.add_subplot(2, 3, 1)
    ax.imshow(rgb_img)
    ax.set_title('RGB')
    ax.axis('off')
    
    if depth_img is not None:
        ax = fig.add_subplot(2, 3, 2)
        ax.imshow(depth_img, cmap='gray')
        ax.set_title('Depth')
        ax.axis('off')

    ax = fig.add_subplot(2, 3, 3)
    ax.imshow(rgb_img)
    for g in gs:
        g.plot(ax)
    ax.set_title('Grasp')
    ax.axis('off')
    
    ax = fig.add_subplot(2, 3, 4)
    plot = ax.imshow(grasp_q_img, cmap='jet', vmin=0, vmax=1)
    ax.set_title('Q')
    ax.axis('off')
    plt.colorbar(plot)

    ax = fig.add_subplot(2, 3, 5)
    plot = ax.imshow(grasp_angle_img, cmap='hsv', vmin=-np.pi / 2, vmax=np.pi / 2)
    ax.set_title('Angle')
    ax.axis('off')
    plt.colorbar(plot)
    ax = fig.add_subplot(2, 3, 6)
    plot = ax.imshow(grasp_width_img, cmap='jet', vmin=0, vmax=100)
    ax.set_title('Width')
    ax.axis('off')
    
    plt.colorbar(plot)
    
    plt.pause()
    fig.canvas.draw()


def plot_grasp(
        fig,
        grasps=None,
        save=False,
        rgb_img=None,
        grasp_q_img=None,
        grasp_angle_img=None,
        no_grasps=1,
        grasp_width_img=None
):
    """
    Plot the output grasp of a network
    :param fig: Figure to plot the output
    :param grasps: grasp pose(s)
    :param save: Bool for saving the plot
    :param rgb_img: RGB Image
    :param grasp_q_img: Q output of network
    :param grasp_angle_img: Angle output of network
    :param no_grasps: Maximum number of grasps to plot
    :param grasp_width_img: (optional) Width output of network
    :return:
    """
    if grasps is None:
        grasps = detect_grasps(grasp_q_img, grasp_angle_img, width_img=grasp_width_img, no_grasps=no_grasps)

    plt.ion()
    plt.clf()

    ax = plt.subplot(111)
    ax.imshow(rgb_img)
    for g in grasps:
        g.plot(ax)
    ax.set_title('Grasp')
    ax.axis('off')
    #plt.show()
    plt.pause(0.1)
    fig.canvas.draw()


    if save:
        time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        fig.savefig('results/{}.png'.format(time))


def save_results(ggt,rgb_img, grasp_q_img, grasp_angle_img, no_grasps=1, grasp_width_img=None, image_num=None): 
    """
    Plot the output of a network
    :param rgb_img: RGB Image
    :param depth_img: Depth Image
    :param grasp_q_img: Q output of network
    :param grasp_angle_img: Angle output of network
    :param no_grasps: Maximum number of grasps to plot
    :param grasp_width_img: (optional) Width output of network
    :return:
    """
    gs = detect_grasps(grasp_q_img, grasp_angle_img, width_img=grasp_width_img, no_grasps=no_grasps) 
    ground_truth_bbox=evaluation.choosen_gtbbx(gs[0],grasp_q_img,grasp_angle_img,ggt,threshold=0.25,no_grasps=1,grasp_width=grasp_width_img)
    fig = plt.figure(figsize=(10,10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    ax.imshow(rgb_img)
    ax.set_title('RGB')
    ax.axis('off')
    fig.savefig('results/rgb %d.png'%(image_num))
    fig.savefig('results/depth.png')
    

    #if depth_img.any():
     #  fig = plt.figure(figsize=(10, 10))
      # plt.ion()
      # plt.clf()
      # ax = plt.subplot(111)
      # ax.imshow(depth_img, cmap='gray')
      # for g in gs:
      #     g.plot(ax)
      # ax.set_title('Depth')
      # ax.axis('off')
      # fig.savefig('results/depth.png')

    fig = plt.figure(figsize=(10, 10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    ax.imshow(rgb_img)
    for g in gs:
        g.plot(ax)
        print("angle of predicted rectangle: {}".format(g.angle*(180/3.14)))
    
    print("angle of ground truth rectangle: {}\n".format(ground_truth_bbox.angle*(180/3.14)))
    ground_truth_bbox.plot(ax)
    ax.set_title('Grasp')
    ax.axis('off')
    fig.savefig('results/grasp %d.png'%(image_num))
    fig.savefig('results/grasp.png')
    
    fig = plt.figure(figsize=(10, 10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    plot = ax.imshow(grasp_q_img, cmap='jet', vmin=0, vmax=1)
    ax.set_title('Q')
    ax.axis('off')
    plt.colorbar(plot)
    fig.savefig('results/quality %d.png'%(image_num))
    
    fig = plt.figure(figsize=(10, 10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    plot = ax.imshow(grasp_angle_img, cmap='hsv', vmin=-np.pi / 2, vmax=np.pi / 2)
    ax.set_title('Angle')
    ax.axis('off')
    plt.colorbar(plot)
    fig.savefig('results/angle %d.png'%(image_num))
    fig = plt.figure(figsize=(10, 10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    plot = ax.imshow(grasp_width_img, cmap='jet', vmin=0, vmax=100)
    ax.set_title('Width')
    ax.axis('off')
    plt.colorbar(plot)
    fig.savefig('results/width %d.png'%(image_num))
    fig.canvas.draw()
    
    #plt.pause(20)
    plt.close(fig)



def visualize_grasps(ggt,rgb_img, grasp_q_img, grasp_angle_img,iou,result,image_num, no_grasps=1, grasp_width_img=None): 
    """
    Plot the output of a network
    :param rgb_img: RGB Image
    :param depth_img: Depth Image
    :param grasp_q_img: Q output of network
    :param grasp_angle_img: Angle output of network
    :param no_grasps: Maximum number of grasps to plot
    :param grasp_width_img: (optional) Width output of network
    :return:
    """
    
    gs = detect_grasps(grasp_q_img, grasp_angle_img, width_img=grasp_width_img, no_grasps=no_grasps)
    print(gs)
    ground_truth_bbox=evaluation.choosen_gtbbx(gs[0],grasp_q_img,grasp_angle_img,
       ggt,threshold=iou,no_grasps=1,grasp_width=grasp_width_img)
    
    fig = plt.figure(figsize=(10,10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    ax.imshow(rgb_img)
    
    
    for g in gs:
        g.plot(ax)
        print("angle of predicted rectangle: {}".format(g.angle*(180/3.14)))
    ax.text(100,0,"PG Angle: {:.2f}\n".format(g.angle*(180/3.14)) , fontsize=10,color='blue')
   # print(g)
    
    #if(ggt.__init__()):      
     # ground_truth_bbox=evaluation.choosen_gtbbx(gs[0],grasp_q_img,grasp_angle_img,
      # ggt,threshold=iou,no_grasps=1,grasp_width=grasp_width_img)
      #print("angle of ground truth rectangle: {}\n".format(ground_truth_bbox.angle*(180/3.14)))
      #ground_truth_bbox.plot(ax)
      #ax.text(0, 0,"GT Angle: {:.2f}\n".format(ground_truth_bbox.angle*(180/3.14)) , fontsize=10,color='orange')
    ax.text(200,0," Angle Diff.: {:.2f}\n".format(abs((abs(g.angle*(180/3.14)))-(abs(ground_truth_bbox.angle*(180/3.14))))) , fontsize=10,color='blue')    
    
    print("angle of ground truth rectangle: {}\n".format(ground_truth_bbox.angle*(180/3.14)))
    ground_truth_bbox.plot(ax)
    ax.text(0, 0,"GT Angle: {:.2f}\n".format(ground_truth_bbox.angle*(180/3.14)) , fontsize=10,color='orange')
    ax.text(300,0,"IOU: {:.2f}\n".format(iou) , fontsize=10,color='black')
    ax.text(100,20,"Prediction: {}\n".format(result) , fontsize=10,color='white')
    ax.set_title('Grasp {}'.format(image_num),y=-0.01)
    ax.axis('off')
    #fig.canvas.draw()
    plt.show(block=True) 
    fig.savefig('results/grasp %d.png'%(image_num))   
    #plt.pause(1)
   # plt.close(fig)


def visualize_grasps_ang(ggt,rgb_img, grasp_q_img, grasp_angle_img,iou,result,image_num, no_grasps=1, grasp_width_img=None): 
    """
    Plot the output of a network
    :param rgb_img: RGB Image
    :param depth_img: Depth Image
    :param grasp_q_img: Q output of network
    :param grasp_angle_img: Angle output of network
    :param no_grasps: Maximum number of grasps to plot
    :param grasp_width_img: (optional) Width output of network
    :return:
    """
    
    gs = detect_grasps(grasp_q_img, grasp_angle_img, width_img=grasp_width_img, no_grasps=no_grasps)
    print(gs)
    ground_truth_bbox=evaluation.choosen_gtbbx(gs[0],grasp_q_img,grasp_angle_img,
       ggt,threshold=iou,no_grasps=1,grasp_width=grasp_width_img)
    #ground_truth_bbox=gtbbs;
    fig = plt.figure(figsize=(10,10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    ax.imshow(rgb_img)
    
    
    for g in gs:
        if (abs(ground_truth_bbox.angle*(180/3.14)) > abs(g.angle*(180/3.14))):
           print(g.angle*(180/3.14))
           g.angle=abs(g.angle) +  ((abs((abs(g.angle*(180/3.14)))-(abs(ground_truth_bbox.angle*(180/3.14))))*3.14)/180)
           print(g.angle*(180/3.14))
           print(abs((abs(g.angle*(180/3.14)))-(abs(ground_truth_bbox.angle*(180/3.14)))))
        elif(abs(ground_truth_bbox.angle*(180/3.14)) < (g.angle*(180/3.14))):
           g.angle =abs(g.angle)  ((abs((abs(g.angle*(180/3.14)))-(abs(ground_truth_bbox.angle*(180/3.14))))*3.14)/180)
        g.plot(ax)
        print("angle of predicted rectangle: {}".format(g.angle*(180/3.14)))
    ax.text(100,0,"PG Angle: {:.2f}\n".format(g.angle*(180/3.14)) , fontsize=10,color='blue')
   # print(g)
    
    #if(ggt.__init__()):      
     # ground_truth_bbox=evaluation.choosen_gtbbx(gs[0],grasp_q_img,grasp_angle_img,
      # ggt,threshold=iou,no_grasps=1,grasp_width=grasp_width_img)
      #print("angle of ground truth rectangle: {}\n".format(ground_truth_bbox.angle*(180/3.14)))
      #ground_truth_bbox.plot(ax)
      #ax.text(0, 0,"GT Angle: {:.2f}\n".format(ground_truth_bbox.angle*(180/3.14)) , fontsize=10,color='orange')
    ax.text(200,0," Angle Diff.: {:.2f}\n".format(abs((abs(g.angle*(180/3.14)))-(abs(ground_truth_bbox.angle*(180/3.14))))) , fontsize=10,color='blue')    
    
    print("angle of ground truth rectangle: {}\n".format(ground_truth_bbox.angle*(180/3.14)))
    ground_truth_bbox.plot(ax)
    ax.text(0, 0,"GT Angle: {:.2f}\n".format(ground_truth_bbox.angle*(180/3.14)) , fontsize=10,color='orange')
    ax.text(300,0,"IOU: {:.2f}\n".format(iou) , fontsize=10,color='black')
    ax.text(100,20,"Prediction: {}\n".format(result) , fontsize=10,color='white')
    ax.set_title('Grasp {}'.format(image_num),y=-0.01)
    ax.axis('off')
    #fig.canvas.draw()
    plt.show(block=True) 
    fig.savefig('results/grasp %d.png'%(image_num))   
    #plt.pause(1)
   # plt.close(fig)


def visualize_grasps_(rgb_img, grasp_q_img, grasp_angle_img,ggt =None,iou_threshold = None,result_iou=None,result = None,image_num=None, no_grasps=1, grasp_width_img=None): 
    """
    Plot the output of a network
    :param rgb_img: RGB Image
    :param depth_img: Depth Image
    :param grasp_q_img: Q output of network
    :param grasp_angle_img: Angle output of network
    :param no_grasps: Maximum number of grasps to plot
    :param grasp_width_img: (optional) Width output of network
    :return:
    """
    
    gs = detect_grasps(grasp_q_img, grasp_angle_img, width_img=grasp_width_img, no_grasps=no_grasps)
    print(result_iou)
    if(ggt is not None):
      ground_truth_bbox=evaluation.choosen_gtbbx(gs[0],grasp_q_img,grasp_angle_img,
        ggt,threshold=iou_threshold,no_grasps=1,grasp_width=grasp_width_img)
    #ground_truth_bbox=gtbbs;
    fig = plt.figure(figsize=(10,10))
    plt.ion()
    plt.clf()
    ax = plt.subplot(111)
    ax.imshow(rgb_img)
    
    
    for g in gs:
        g.plot(ax)
        print("angle of predicted rectangle: {}".format(g.angle*(180/3.14)))
    ax.text(100,0,"PG Angle: {:.2f}\n".format(g.angle*(180/3.14)) , fontsize=10,color='blue')
   # print(g)
    
    #if(ggt.__init__()):      
     # ground_truth_bbox=evaluation.choosen_gtbbx(gs[0],grasp_q_img,grasp_angle_img,
      # ggt,threshold=iou,no_grasps=1,grasp_width=grasp_width_img)
      #print("angle of ground truth rectangle: {}\n".format(ground_truth_bbox.angle*(180/3.14)))
      #ground_truth_bbox.plot(ax)
      #ax.text(0, 0,"GT Angle: {:.2f}\n".format(ground_truth_bbox.angle*(180/3.14)) , fontsize=10,color='orange')
    if(ggt is not None):
      ax.text(200,0," Angle Diff.: {:.2f}\n".format(abs((abs(g.angle*(180/3.14)))-(abs(ground_truth_bbox.angle*(180/3.14))))) , fontsize=10,color='blue')    
    
      print("angle of ground truth rectangle: {}\n".format(ground_truth_bbox.angle*(180/3.14)))
      ground_truth_bbox.plot(ax)
      ax.text(0, 0,"GT Angle: {:.2f}\n".format(ground_truth_bbox.angle*(180/3.14)) , fontsize=10,color='orange')
      ax.text(300,0,"IOU: {:.2f}\n".format(result_iou) , fontsize=10,color='black')
      ax.text(100,20,"Prediction: {}\n".format(result) , fontsize=10,color='white')
    ax.set_title('Grasp {}'.format(image_num),y=-0.01)
    ax.axis('off')
    #fig.canvas.draw()
    plt.show(block=True) 
    fig.savefig('results/grasp %d.png'%(image_num))   
    #plt.pause(1)
   # plt.close(fig)



